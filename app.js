const fs = require('fs')
const csv = require("csvtojson");
const matches_filePath = "./csv_files/matches.csv"
const deliveries_filePath = "./csv_files/deliveries.csv"

csv()
.fromFile(matches_filePath)
.then(matches => {
    fs.writeFileSync("matches.json", JSON.stringify(matches), "utf-8", err => {
        if (err){
            console.log(err)
        }
    })
})

csv()
.fromFile(deliveries_filePath)
.then(deliveries => {
    fs.writeFileSync("deliveries.json", JSON.stringify(deliveries), "utf-8", err => {
        if (err){
            console.log(err)
        }
    })
})

    

