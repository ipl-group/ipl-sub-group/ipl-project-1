const fs = require('fs');

const data = fs.readFileSync("../data/matches.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
})

let matches = JSON.parse(data)

const matchesWonPerTeamPerYear = (matches)=>{
    let result = {}

    for(let match of matches){
        const season = match.season;
        const winner = match.winner;
        if(result[season]){
            if(result[season][winner]){
                result[season][winner]+=1
            }else{
                result[season][winner]=1
            }

        }else{
            result[season] = {}
        }
    }
    console.log(result);
    return result
}

const result = matchesWonPerTeamPerYear(matches)

fs.writeFileSync("../output/matchesWonPerTeamPerYear.json", JSON.stringify(result), "utf-8", err=>{
    console.log(err)
})