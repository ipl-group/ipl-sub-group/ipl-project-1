const fs = require('fs');

const dataDelivery = fs.readFileSync("../data/deliveries.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
})

const data = fs.readFileSync("../data/matches.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
})

let deliveries = JSON.parse(dataDelivery)
let matches = JSON.parse(data);

const economicalBowlersIn2015 = (matches, deliveries)=>{
        let deliveriesIn2015=[]
        for(let match of matches){
            if(match.season === '2015'){
                for(let delivery of deliveries){
                    if (match.id === delivery.match_id && parseInt(delivery.total_runs) >= 1){
                        deliveriesIn2015.push(delivery)
                    }
                }
            }
        }
        
        let bowlers = {}
        for (let delivery of deliveriesIn2015){
            if(bowlers[delivery.bowler]){
                bowlers[delivery.bowler].runs += parseInt(delivery.total_runs)
                bowlers[delivery.bowler].balls +=1

            }else{
                bowlers[delivery.bowler] = {
                    'balls':1,
                    'runs':parseInt(delivery.total_runs)
                }
            }
        }
        
        const bowlersArray = Object.entries(bowlers)
       
        let economicBowlers={}
        for(let item of bowlersArray){
            economicBowlers[item[0]] = parseInt((item[1].runs/(item[1].balls/6)).toFixed(2))
        }

        const economicBowlersArray = Object.entries(economicBowlers)
        
        economicBowlersArray.sort(function (a,b){
            return b[1] - a[1]
        })

        let topTenEconomicBowlers = []
        for(let i=0 ; i< 10; i++){
            topTenEconomicBowlers.push(economicBowlersArray[i]);  
        }
        
        topTenEconomicBowlers =  Object.fromEntries(topTenEconomicBowlers)

        console.log(topTenEconomicBowlers)

        return topTenEconomicBowlers;
}

const result = economicalBowlersIn2015(matches, deliveries)

fs.writeFileSync("../output/economicalBowlersIn2015.json", JSON.stringify(result), "utf-8", err =>{
    console.log(err)
});

