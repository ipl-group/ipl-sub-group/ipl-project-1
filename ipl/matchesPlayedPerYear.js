const fs = require('fs');

const data = fs.readFileSync("../data/matches.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
})

let matches = JSON.parse(data)
const matchesPlayedPerYear = (matches)=>{
    let result = {}
    for(let match of matches){
        const season = match.season
        if(result[season]){
            result[season]+=1
        }else{
            result[season]=1
        }
    }
    return result
}

const result = matchesPlayedPerYear(matches);

fs.writeFileSync("../output/matchesPlayedPerYear.json", JSON.stringify(result), "utf-8", (err)=>{
    console.log(err)
})