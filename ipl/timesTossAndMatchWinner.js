const fs = require('fs');

const data = fs.readFileSync("../data/matches.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
})

let matches = JSON.parse(data);

const timesTossAndMatchWinner = (matches)=>{
       let timesMatchAndTossWinner={}
       for(let match of matches){
          if(match.toss_winner === match.winner){
                if(timesMatchAndTossWinner[match.winner]){
                    timesMatchAndTossWinner[match.winner]+=1
                }else{
                    timesMatchAndTossWinner[match.winner] = 1
                }
          }
       }

     return timesMatchAndTossWinner
}

const result = timesTossAndMatchWinner(matches)

fs.writeFileSync("../output/timesTossAndMatchWinner.json", JSON.stringify(result), "utf-8", err => {
    console.log(err)
})