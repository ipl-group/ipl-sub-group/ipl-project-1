const fs  = require("fs")

const data = fs.readFileSync("../data/matches.json", "utf-8")

const matches = JSON.parse(data)

const dataDelivery = fs.readFileSync("../data/deliveries.json", "utf-8")

const deliveries = JSON.parse(dataDelivery);

const strikeRateBatsmanInSeason = (matches, deliveries)=>{
        const seasonsArray = new Set(matches.map((match) => match.season))
        
        let strikeRateObj={}
        for (let season of seasonsArray){
           
            let strikeRateBatsman = {}
            for(let match of matches){
                if (match.season === season){
                    
                   
                    for (let delivery of deliveries){
                        if(delivery.match_id === match.id){
                            
                            if(strikeRateBatsman[delivery.batsman]){
                                strikeRateBatsman[delivery.batsman].runs += parseInt(delivery.total_runs)
                                strikeRateBatsman[delivery.batsman].balls +=1
                            }else{
                                strikeRateBatsman[delivery.batsman] = {
                                    runs: parseInt(delivery.total_runs),
                                    balls:1
                                }
                            }
                        }
                    }

                   
                }
              
            }
            // console.log(strikeRateBatsman)
            Object.keys(strikeRateBatsman).map(batsman => {
                    strikeRateBatsman[batsman]= parseInt(Number((strikeRateBatsman[batsman].runs/ strikeRateBatsman[batsman].balls)*100).toFixed(2))
            })

            strikeRateObj[season] = strikeRateBatsman
            
        }
        return strikeRateObj
}

const result = strikeRateBatsmanInSeason(matches, deliveries)
console.log(result);

fs.writeFileSync("../output/strikeRateBatsmanInSeason.json", JSON.stringify(result), "utf-8", err => {
    console.log(err.message);
})