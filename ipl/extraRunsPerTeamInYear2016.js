const fs = require('fs');

const data = fs.readFileSync("../data/matches.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
});

const dataDelivery = fs.readFileSync("../data/deliveries.json", "utf-8", err =>{
    if(err){
        console.log(err)
    }
});

let matches = JSON.parse(data);
let deliveries = JSON.parse(dataDelivery);

const extraRunsPerTeamInYear2016=(matches, deliveries)=>{
          let extraRunsPerTeam={}      

          for (let match of matches){
            if (match.season === '2016'){
                for (let delivery of deliveries){
                    if (match.id === delivery.match_id){
                             if(parseInt(delivery.extra_runs) >=1){
                                   if(extraRunsPerTeam[delivery.bowling_team]){
                                         extraRunsPerTeam[delivery.bowling_team] += parseInt(delivery.extra_runs)
                                   }else{
                                         extraRunsPerTeam[delivery.bowling_team] = parseInt(delivery.extra_runs)
                                   }
                             }
                    }
                }
            }
          }

          return extraRunsPerTeam
}

const result =  extraRunsPerTeamInYear2016(matches, deliveries);
fs.writeFileSync("../output/extraRunsPerTeamInYear2016.json", JSON.stringify(result), "utf-8", (err)=>{
    console.log(err)
});
