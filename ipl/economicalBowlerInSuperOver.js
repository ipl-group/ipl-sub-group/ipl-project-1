const fs = require("fs");

const data = fs.readFileSync("../data/deliveries.json", "utf-8", err => {
    console.log(err)
})

const deliveries = JSON.parse(data);

let bowlerPerSuperOver = {}
deliveries.map(delivery => {
    if(delivery.is_super_over !== '0'){
       
        if(bowlerPerSuperOver[delivery.bowler]){
            bowlerPerSuperOver[delivery.bowler].runs += parseInt(delivery.total_runs) 
            bowlerPerSuperOver[delivery.bowler].balls += 1

        }else{
            bowlerPerSuperOver[delivery.bowler] = {}
            bowlerPerSuperOver[delivery.bowler].runs = parseInt(delivery.total_runs) 
            bowlerPerSuperOver[delivery.bowler].balls = 1 
        }
    }
})

const economicalBowlers = Object.keys(bowlerPerSuperOver).map(bowler => {
     const economy = ((bowlerPerSuperOver[bowler].runs / bowlerPerSuperOver[bowler].balls)*100).toFixed(2)
     return {bowler, economy}
})

const result = economicalBowlers.sort((a,b)=>{
    if( parseInt(a.economy) < parseInt(b.economy)){
        return -1
    }else if(parseInt(a.economy) > parseInt(b.economy)){
        return 1
    }else{
        return 0
    }
})

const economicBowler = result.slice(-1)[0]

fs.writeFileSync("../output/economicalBowlerInSuperOver.json", JSON.stringify(economicBowler), "utf-8", err => {
    console.log(err.message)
});
