const fs = require("fs");

const data = fs.readFileSync("../data/matches.json", "utf-8", err => {
    console.log(err)
});

const matches = JSON.parse(data);

const playerOfMathcInEachSeason = (matches)=>{

      const seasonsSet = new Set(matches.map((match)=>match.season))

      let playerOfSeason = {}
      for(let year of seasonsSet){

         let awardsInSeason ={}
         for(let match of matches){
            
            if(match.season === year){
               if(match.player_of_match in awardsInSeason){
                  awardsInSeason[match.player_of_match] += 1
               }else{
                  awardsInSeason[match.player_of_match] = 1
               }
            }

         }
         playerOfSeason[year] = awardsInSeason;
      }
       const playerInSeason = {}
       for(let season in playerOfSeason){
         const arr = Object.entries(playerOfSeason[season])
         arr.sort((a,b)=> b[1]-a[1])
         const player = Object.fromEntries([arr[0]])
         playerInSeason[season]= player
         }

         return playerInSeason;
       }

      


const result = playerOfMathcInEachSeason(matches)

fs.writeFileSync("../output/playerOfMathcInEachSeason.json", JSON.stringify(result), "utf8", err => {
   console.log(err)
});
