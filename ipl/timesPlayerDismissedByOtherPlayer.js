const fs = require("fs");

const data = fs.readFileSync("../data/deliveries.json", "utf-8", err => {
    console.log(err.message);
})

const deliveries = JSON.parse(data);

let dismissalObj = {}

deliveries.map(delivery => {
    if (delivery.player_dismissed !== ""){
        if (delivery.player_dismissed in dismissalObj){
            if(dismissalObj[delivery.player_dismissed][delivery.bowler]){
                dismissalObj[delivery.player_dismissed][delivery.bowler] +=1
            }else{
                dismissalObj[delivery.player_dismissed][delivery.bowler] = 1
            }
                
        }else{
            dismissalObj[delivery.player_dismissed]={}
            dismissalObj[delivery.player_dismissed][delivery.bowler] = 1
        }
    }
})

const dismissedCount = Object.keys(dismissalObj).map(dismissedBatsman => {
   const dismissedCountObj =  Object.entries(dismissalObj[dismissedBatsman]).sort((a,b) => b[1]-a[1]).slice(0,1).flat(1)
   return [dismissedBatsman, dismissedCountObj]
})

const HighestDismissedCount = dismissedCount.sort((a,b) => b[1][1]-a[1][1]).slice(0,1).flat(1)
const dismissedObj = {}
dismissedObj["dismissedBowler"] = HighestDismissedCount[1][0]
dismissedObj["dismissedCount"] = HighestDismissedCount[1][1]

const HighestDismissedBatsman = {}
HighestDismissedBatsman[HighestDismissedCount[0]] = dismissedObj


fs.writeFileSync("../output/timesPlayerDismissedByOtherPlayer.json", JSON.stringify(HighestDismissedBatsman), "utf-8", err =>{
    console.log(err)
})
