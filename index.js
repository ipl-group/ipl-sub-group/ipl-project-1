document.addEventListener("DOMContentLoaded", async()=>{
    const data = await fetch('../output/economicalBowlersIn2015.json')
    .then((response)=> response.json())
    .then(data=>{
        return data
    });

    const bowlers = Object.keys(data)
    const economy = Object.values(data)

    Highcharts.chart('EconomyBowlersIn2015', {
      title:{
        text:"Economical Bowlers in 2015",
      },

      chart:{
        type:'column'
      },

      xAxis: {
        title:{
          text:"BowlerNames",
          style:{
            fontSize:"16px",
            color:"Black",
          }
        },
        labels: {
            style: {
                fontSize:"12px",
                color: "black",
            }
        },
        categories: bowlers
      },

      yAxis:{
        title:{
            text:"Economy",
            style:{
                fontSize:"16px",
                color:"Black",
              },
        },
        labels: {
            style: {
                fontSize:"16px",
                color: "black",
            }
        },
      },

      series:[
        {
            data:economy,
            dataLabels:{
                enabled:true,
                y: 20, // 10 pixels down from the top,    
                style:{
                    fontSize:"12px"
                }        
        }
    }
      ]
    });
});



document.addEventListener("DOMContentLoaded", async()=>{
  const data = await fetch("../output/extraRunsPerTeamInYear2016.json")
                      .then((response)=> response.json())
                      .then((data)=>{
                         return data
                      });
  const teams = Object.keys(data);
  const extraRuns = Object.values(data);

  Highcharts.chart('ExtraRunsIn2016', {
    title:{
      text:"Extra Runs Per Team In 2016",
    },

    chart:{
      type:"column"
    },

    xAxis:{
      title:{
        text:"Teams",
        style:{
          fontSize:"18px",
          color:"Black",
        }
      },labels: {
        style: {
            fontSize:"12px",
            color: "black",
            rotation:5,
        }
    },
      categories:teams
    },

    yAxis:{
      title:{
          text:"Extra Runs",
          style:{
              fontSize:"18px",
              color:"Black",
            },
      },
      labels: {
          style: {
              fontSize:"10px",
              color: "black",
          }
      },
    },

    series:[
      {
        data:extraRuns,
        dataLabels:{
          enabled:true,
          y:15,
        }
      }
    ]
  })
});


document.addEventListener("DOMContentLoaded", async()=>{
  const data = await fetch("../output/matchesPlayedPerYear.json")
                      .then((response)=> response.json())
                      .then((data)=>{
                         return data
                      });
  const years = Object.keys(data);
  const matches = Object.values(data);

  Highcharts.chart('matchesPlayedPerYear', {
    title:{
      text:"Matches Played Per Year",
    },

    chart:{
      type:"column"
    },

    xAxis:{
      title:{
        text:"Years",
        style:{
          fontSize:"18px",
          color:"Black",
        }
      },labels: {
        style: {
            fontSize:"12px",
            color: "black",
            rotation:5,
        }
    },
      categories:years
    },

    yAxis:{
      title:{
          text:"Extra Runs",
          style:{
              fontSize:"18px",
              color:"Black",
            },
      },
      labels: {
          style: {
              fontSize:"10px",
              color: "black",
          }
      },
    },

    series:[
      {
        data:matches,
        dataLabels:{
          enabled:true,
          y:15,
        }
      }
    ]
  })
});


document.addEventListener("DOMContentLoaded", async()=>{
  const data = await fetch("../output/matchesWonPerTeamPerYear.json")
                      .then((response)=> response.json())
                      .then((data)=>{
                         return data
                      });
  const years = Object.keys(data);
  const allTeams = []
  for(let item in data){
    for(let teamName in data[item]){
      if(! allTeams.includes(teamName) && teamName !==""){
        allTeams.push(teamName)
      }
    }
  }


  
  const seriesData = []
  for(let item of allTeams){
    let arr = []
    for (let item1 in data){
        if(data[item1][item]){
          arr.push(data[item1][item])
        }else{
          arr.push(null)
        }
    }
    let obj = {
      'name':item,
      'data':arr,
      dataLabels:{
        enabled:true
      }
    }
    seriesData.push(obj)
  }


  Highcharts.chart('matchesWonPerTeamPerYear', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Matches Won Per Team Per Year'
    },
    xAxis: {
      categories: years,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Matches Won',
        style:{
          fontSize:"18px",
          color:"Black",
        },
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: seriesData
  });
});



document.addEventListener("DOMContentLoaded", async()=>{
  const data = await fetch("../output/strikeRateBatsmanInSeason.json")
                      .then((response)=> response.json())
                      .then((data)=>{
                         return data
                      });
  const years = Object.keys(data);
  const allBatsmans = []
  for(let item in data){
    for(let teamName in data[item]){
      if(! allBatsmans.includes(teamName) && teamName !==""){
        allBatsmans.push(teamName)
      }
    }
  }

  
  const seriesData = []
  for(let item of allBatsmans){
    let arr = []
    for (let item1 in data){
        if(data[item1][item]){
          arr.push(data[item1][item])
        }else{
          arr.push(null)
        }
    }
    let obj = {
      'name':item,
      'data':arr,
      dataLabels:{
        enabled:true
      }
    }
    seriesData.push(obj)
  }
 

  Highcharts.chart('strikeRateBatsmanInSeason', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Strike Rate of Batsmans In Each Season'
    },
    xAxis: {
      categories: years,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Strike Rate',
        style:{
          fontSize:"18px",
          color:"Black",
        },
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: seriesData
  });
});



document.addEventListener("DOMContentLoaded", async()=>{
  const data = await fetch("../output/playerOfMathcInEachSeason.json")
                      .then((response)=> response.json())
                      .then((data)=>{
                         return data
                      });

  const years = Object.keys(data);
  const allPlayers = []
  for(let item in data){
    for(let player in data[item]){
      if(! allPlayers.includes(player)){
        allPlayers.push(player)
      }
    }
  }

  
  const seriesData = []
  for(let item of allPlayers){
    let arr = []
    for (let item1 in data){
        if(data[item1][item]){
          arr.push(data[item1][item])
        }else{
          arr.push(null)
        }
    }
    
    let obj = {
      'name':item,
      'data':arr,
      dataLabels:{
        enabled:true
      }
    }
    seriesData.push(obj)
  }

  Highcharts.chart('playerInSeason', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Strike Rate of Batsmans In Each Season'
    },
    xAxis: {
      categories: years,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Strike Rate',
        style:{
          fontSize:"18px",
          color:"Black",
        },
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: seriesData
  });
});



document.addEventListener("DOMContentLoaded", async()=>{
  const data = await fetch('../output/timesTossAndMatchWinner.json')
  .then((response)=> response.json())
  .then(data=>{
      return data
  });
  console.log(data);

  const teams = Object.keys(data)
  const wins = Object.values(data)

  Highcharts.chart('tossAndMatchWinner', {
    title:{
      text:"Toss And Match Winners",
    },

    chart:{
      type:'column'
    },

    xAxis: {
      title:{
        text:"Teams",
        style:{
          fontSize:"16px",
          color:"Black",
        }
      },
      labels: {
          style: {
              fontSize:"12px",
              color: "black",
          }
      },
      categories: teams
    },

    yAxis:{
      title:{
          text:"Wins",
          style:{
              fontSize:"16px",
              color:"Black",
            },
      },
      labels: {
          style: {
              fontSize:"16px",
              color: "black",
          }
      },
    },

    series:[
      {
          data:wins,
          dataLabels:{
              enabled:true,
              y: 20, // 10 pixels down from the top,    
              style:{
                  fontSize:"12px"
              }        
      }
  }
    ]
  });
});


